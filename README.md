# Rasa-for-cobat

A platform for designing and teaching AI assistant built base on the Rasa Framework

## Installation

1. Rasa uses **Poetry** to package and manage dependencies so you'll need to install **Poetry**:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

2. Clone this repository:

```bash
git clone https://gitlab.com/phamnam.mta/rasa-for-cobat.git
cd rasa-for-cobat-v2/rasa-for-cobat
```

3. Install dependencies:

```bash
poetry install
pip install -r addons_requirements.txt
```

4. Run rasa server with command:

```bash
rasa run --model agents/default/models --endpoints agents/default/endpoints.yml --credentials agents/default/credentials.yml --enable-api --debug
```

## Features

- Agent

- Train Dialogs

## Documents

Updating

## Usage

Updating
