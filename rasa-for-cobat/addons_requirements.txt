requests==2.23.0
requests_futures==0.9.9
fuzzy_matcher==0.1.0
fbmessenger==6.00
sgqlc==9.1
tensorflow-text==2.3.0
wget==3.2