from setuptools import setup, find_packages
import os
import io

setup(
    name="rasa_addons",
    version="2.1.3.6",
    author="Botfront",
    description="Rasa Addons - Components for Rasa and Botfront",
    long_description_content_type="text/markdown",
    install_requires=[
        "requests",
        "requests_futures",
        "fuzzy_matcher",
        "fbmessenger",
        "sgqlc",
        "tensorflow-text",
    ],
    packages=find_packages(include=["rasa_addons*"]),
    licence="Apache 2.0",
    url="https://botfront.io",
    author_email="hi@botfront.io",
)
